import arcade

SCREEN_WIDTH = 640
SCREEN_HEIGHT = 512
NOTE_1 = arcade.key.Q
NOTE_2 = arcade.key.W
NOTE_3 = arcade.key.O 
NOTE_4 = arcade.key.P
NOTE_COLOURS = arcade.color.RED, arcade.color.YELLOW, arcade.color.GREEN, arcade.color.BLUE

window = arcade.Window(SCREEN_WIDTH, SCREEN_HEIGHT, 'QWOP Beat', antialiasing=False)
