import arcade, arcade.gui, json
from consts import *
from game import *

class ChartEditor(arcade.View):
	def __init__(self, song, chart=None):
		super().__init__()
		if not chart:
			with open(f'charts/{song}.json', 'r') as f:
				self.chart = json.load(f)
		else:
			self.chart = chart
		self.chart_change_y = 0
		self.camera = None
		self.ui_camera = arcade.Camera(SCREEN_WIDTH, SCREEN_HEIGHT)
		self.ui_manager = None
		self.song_input = arcade.gui.UIInputText(500, 380, text=self.chart['song'], height=20)
		self.tempo_input = arcade.gui.UIInputText(500, 350, text=str(self.chart['tempo']), height=20)
		self.mouse_x = 0
		self.mouse_y = 0
		self.music = None
	
	def setup(self):
		save_button = arcade.gui.UIFlatButton(400, 300, text='Save', width=200, height=40)
		reload_button = arcade.gui.UIFlatButton(400, 250, text='Reload', width=200, height=40)
		save_button.on_click = self.on_save_clicked
		reload_button.on_click = self.on_reload_clicked
		self.ui_manager.add(save_button)
		self.ui_manager.add(reload_button)
		self.ui_manager.add(arcade.gui.UIPadding(self.song_input, bg_color=arcade.color.WHITE))
		self.ui_manager.add(arcade.gui.UIPadding(self.tempo_input, bg_color=arcade.color.WHITE))
		self.camera = arcade.Camera(SCREEN_WIDTH, SCREEN_HEIGHT)
		self.camera.move_to((0, 256))
		self.camera.use()
		self.music = arcade.Sound(f'music/{self.chart["song"]}.ogg').play()
		self.music.pause()
	
	def on_draw(self):
		arcade.start_render()
		self.ui_camera.use()
		arcade.draw_text('Arrows to change position\nLeft click to add/change note\nRight click to remove note\nSHIFT-click to add trick note\nENTER to begin', SCREEN_WIDTH-180, 500, arcade.color.WHITE, font_size=10, anchor_x='left', anchor_y='top', multiline=True, width=190)
		arcade.draw_text('  Song:\n\nTempo:', 450, 400, arcade.color.WHITE, font_size=10, anchor_x='left', anchor_y='top', multiline=True, width=100)
		self.ui_manager.draw()
		arcade.draw_rectangle_filled(144, 256, SCREEN_WIDTH/4, SCREEN_HEIGHT, arcade.color.GRAY)
		self.camera.use()
		if self.mouse_x >= 96 and self.mouse_x <= 208:
			arcade.draw_rectangle_filled(self.mouse_x//32*32, self.mouse_y//32*32, 32, 32, arcade.color.WHITE)
		for i in range(len(self.chart['notes'])):
			if note := self.chart['notes'][i]:
				if note <= 4:
					arcade.draw_circle_filled(64+note*32, SCREEN_HEIGHT-32-i*32, 12, NOTE_COLOURS[note-1])
				else:
					arcade.draw_circle_filled(64+(note-4)*32, SCREEN_HEIGHT-32-i*32, 12, arcade.color.DARK_RED)
		self.ui_camera.use()
		arcade.draw_line(0, SCREEN_HEIGHT/2, 240, SCREEN_HEIGHT/2, arcade.color.WHITE, 2)
	
	def on_update(self, delta_time):
		self.chart['song'] = self.song_input.text
		if len(self.tempo_input.text) <= 0:
			self.chart['tempo'] = 1
		else:
			self.chart['tempo'] = int(self.tempo_input.text)
		self.camera.move_to((0,self.camera.position[1]+self.chart_change_y))
		if self.music.playing:
			self.camera.move_to((0,self.camera.position[1]-2.5), 24/(1 / (self.chart['tempo'] / 60))/60)
	
	def on_key_press(self, key, key_modifiers):
		if not key_modifiers:
			if key == arcade.key.UP:
				self.chart_change_y = 2
			elif key == arcade.key.DOWN:
				self.chart_change_y = -2
			elif key == arcade.key.ENTER:
				window.show_view(Game(self.chart['song'], self.chart['tempo'], 1.0, self.chart['notes'], self))
			elif key == arcade.key.SPACE:
				if self.music.playing:
					self.music.pause()
				else:
					print(-(self.camera.position[1]-256)*(1 / (self.chart['tempo'] / 60) / 64))
					self.music.seek(-(self.camera.position[1]-256)*(1 / (self.chart['tempo'] / 60) / 64))
					self.music.play()
		elif key_modifiers & arcade.key.MOD_CTRL:
			if key == arcade.key.S:
				self.on_save_clicked(None)
			elif key == arcade.key.R:
				self.on_reload_clicked(None)
	
	def on_key_release(self, key, key_modifiers):
		if key == arcade.key.UP or key == arcade.key.DOWN:
			self.chart_change_y = 0
	
	def on_mouse_press(self, x, y, button, key_modifiers):
		y += int(self.camera.position[1])
		if x >= 96 and x <= 208:
			if button == arcade.MOUSE_BUTTON_LEFT:
				if not key_modifiers:
					self.chart['notes'][(SCREEN_HEIGHT-y) // 32 + self.chart_change_y] = (x - 64) // 32
				elif key_modifiers & arcade.key.MOD_SHIFT:
					self.chart['notes'][(SCREEN_HEIGHT-y) // 32 + self.chart_change_y] = (x - 64) // 32 + 4
			elif button == arcade.MOUSE_BUTTON_RIGHT:
				self.chart['notes'][(SCREEN_HEIGHT-y) // 32 + self.chart_change_y] = 0
	
	def on_mouse_release(self, x, y, button, key_modifiers):
		pass
	
	def on_mouse_motion(self, x, y, delta_x, delta_y):
		y += int(self.camera.position[1])
		self.mouse_x = x
		self.mouse_y = y
	
	def on_show_view(self):
		self.ui_manager = arcade.gui.UIManager()
		arcade.set_background_color(arcade.color.BLACK)
		self.ui_manager.enable()
		self.setup()
	
	def on_hide_view(self):
		self.ui_manager.disable()
		if self.music:
			arcade.stop_sound(self.music)
	
	def on_save_clicked(self, event):
		print(self.chart)
		with open(f'charts/{self.chart["song"]}.json', 'w') as f:
			json.dump(self.chart, f)
		print('Saved!')
	
	def on_reload_clicked(self, event):
		window.show_view(ChartEditor(song=self.chart["song"]))
		print('Reloaded!')
