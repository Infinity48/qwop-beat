import arcade

class Note(arcade.Sprite):
	def __init__(self, note, scale=1.0):
		super().__init__(None, scale)
		self.textures = arcade.load_spritesheet('images/notes.png', 32, 32, 4, 8)
		self.set_texture(note-1)
		if note >= 5: # Is this a trick note?
			self.center_x = (note-4) * 64
		else:
			self.center_x = note * 64
		self._note = note
	
	@property
	def note(self):
		return self._note