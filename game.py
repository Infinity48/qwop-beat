from consts import *
from classes import Note

class Game(arcade.View):
	def __init__(self, song, tempo, speed, chart, editor):
		super().__init__()

		self.bpm = tempo
		self.beat = 0
		self.beat_time = 0.0
		self.timer = 0.0
		self.score = 0
		self.chart = chart
		self.song = song
		self.editor = editor
		self.scroll_speed = speed
		
		self.camera = arcade.Camera(SCREEN_WIDTH, SCREEN_HEIGHT)
		self.music = None
		self.notes = None
		self.inputs = arcade.SpriteList()
		for i in range(4):
			input_note = arcade.SpriteCircle(24, arcade.color.GRAY)
			input_note.center_x = 64+64*i
			input_note.center_y = 96
			self.inputs.append(input_note)

	def setup(self):
		self.score = 0
		self.beat = 0
		self.beat_time = 1 / (self.bpm / 60)
		self.notes = arcade.SpriteList()
		self.notes.camera = arcade.Camera(SCREEN_WIDTH, SCREEN_HEIGHT)
		print(self.beat_time)
		curnote = 0
		for note in self.chart:
			curnote += 1
			if note:
				newnote = Note(note, 1.5)
				newnote.center_y = 0 + (64 + curnote * 64)
				self.notes.append(newnote)
		self.music = arcade.Sound(f'music/{self.song}.ogg', streaming=True).play()

	def on_draw(self):
		arcade.start_render()
		
		self.notes.camera.use()
		self.inputs.draw()
		self.notes.draw()
		self.camera.use()
		arcade.draw_text(f'Beat: {self.beat}', 25, 480, arcade.color.GREEN, font_size=10, anchor_x='left', anchor_y='top')
		arcade.draw_text(f'Score: {self.score}', 25, 460, arcade.color.GREEN, font_size=10, anchor_x='left', anchor_y='top')

	def on_update(self, delta_time):
		self.timer += delta_time
		for input in self.inputs:
			input.center_y = self.notes.camera.position[1]+96
		if self.music.playing:
			if self.timer >= self.beat_time:
				print('beat')
				self.timer = 0
				self.beat += 1
				for note in self.notes:
					#print(note.center_y)
					if note.center_y < self.notes.camera.position[1]+72:
						note.kill()
			self.notes.camera.move_to((0,self.notes.camera.position[1]+self.scroll_speed), 64/self.beat_time/60)
		else:
			window.show_view(self.editor)

	def on_key_press(self, key, key_modifiers):
		#print(self.chart[self.beat])
		if key == NOTE_1:
			self.check_note(1)
			self.inputs[0].alpha = 127
		elif key == NOTE_2:
			self.check_note(2)
			self.inputs[1].alpha = 127
		elif key == NOTE_3:
			self.check_note(3)
			self.inputs[2].alpha = 127
		elif key == NOTE_4:
			self.check_note(4)
			self.inputs[3].alpha = 127
		elif key == arcade.key.R:
			arcade.stop_sound(self.music)
			self.setup()
		elif key == arcade.key.ESCAPE:
			window.show_view(self.editor)

	def on_key_release(self, key, key_modifiers):
		if key == NOTE_1:
			self.inputs[0].alpha = 255
		elif key == NOTE_2:
			self.inputs[1].alpha = 255
		elif key == NOTE_3:
			self.inputs[2].alpha = 255
		elif key == NOTE_4:
			self.inputs[3].alpha = 255

	def check_note(self, note_hit):
		''' Check if the note hit is correct. '''
		if self.notes[0].note == note_hit and arcade.check_for_collision_with_list(self.notes[0], self.inputs):
			# Hit the note, yay!
			self.notes[0].kill()
			self.score += 1
		else:
			# Missed the note or hit a trick note, subtract score.
			self.score -= 1
	
	def on_hide_view(self):
		arcade.stop_sound(self.music)
	
	def on_show_view(self):
		arcade.set_background_color(arcade.color.AMAZON)
		self.setup()
